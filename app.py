import datetime
import hashlib


class Block:
    blockNum = 0
    data = None
    next = None
    hash = None
    nonce = 0
    prevhash = 0x0
    timestamp = datetime.datetime.now()


    def __init__(self, data):
        self.data = data

    def hash(self):
        h = hashlib.sha256()
        h.update(
        str(self.nonce).encode('utf-8') +
        str(self.data).encode('utf-8') +
        str(self.prevhash).encode('utf-8') +
        str(self.timestamp).encode('utf-8') +
        str(self.blockNum).encode('utf-8')
        )
        return h.hexdigest()

    def __str__(self):
        return "Block hash: " + str(self.hash()) + "\nBlockNum = "+str(self.blockNum)

class Blockchain:
    maxNonce = 2**32
    diff = 10
    target = 2 ** (256-diff)

    block = Block("genesis")

    head = block

    def add(self, block):
        block.prevhash = self.block.hash()
        block.blockNum = self.block.blockNum + 1


        self.block.next = block
        self.block = self.block.next


    def mine(self, block):
        for n in range(self.maxNonce):
            if int(block.hash(), 16) <= self.target:
                self.add(block)
                print(block)
                break
            else:
                block.nonce+=1

bc = Blockchain()
for n in range(10):
    bc.mine(Block("Block" + str(n+1)))



while bc.head != None:
    print(bc.head)
    bc.head = bc.head.next